#!/bin/bash

# Instalacion de repositorio NodeJS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

# Actualizacion de sistema
sudo apt upgrade -y

# Instalacion de dependencias en sistema
sudo apt install -y nodejs build-essential libmodbus-dev make

# Instalacion de manejador de servicios
sudo npm install -g pm2

# Configuracion de manejador de servicios
sudo pm2 startup

# Instalacion de dependencias de proyecto
npm install

# Inicializacion de parametros bases
node init.js

# Compilacion de lector de modbus
make

# Instalacion de servicio
sudo pm2 start index.js
sudo pm2 save
